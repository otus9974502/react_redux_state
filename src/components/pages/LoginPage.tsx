import { useState } from "react";
import { Col, Row } from "react-bootstrap";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import { useDispatch, useSelector } from "react-redux";
import { User } from "../../models/user";
import { Actions } from "../../state/reducers/loginReducer";
import { useNavigate } from "react-router-dom";

export const LoginPage = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState("");

  //регистрируем диспетчер
  const dispatch = useDispatch();
  // подписываемся на стор зарегистрированныхз пользователй
  const usersStore = useSelector((state: any) => state.usersStore.users);
  // регистрируем навигатор
  const navigate = useNavigate();

  const submit = (event: any) => {
    event.preventDefault();

    // ищем в зарегистрированных пользователях совпадение по email и паролю
    const find = usersStore.find(
      (u: User) => u.login == email && u.password == password
    );

    // если такой пользователь найден
    if (find !== undefined) {
      dispatch(Actions.loginUser(find));
      setError("");
      navigate("/");
    } else {
      setError("Не найдено совпадений логина и пароля");
    }
  };

  return (
    <>
      <Row>
        <Col></Col>
        <Col>
          <Form onSubmit={submit}>
            <Form.Group className="mb-3" controlId="formBasicEmail">
              <Form.Label>Email address</Form.Label>
              <Form.Control
                type="email"
                onChange={(e) => {
                  setEmail(e.target.value);
                }}
                placeholder="Enter email"
              />
            </Form.Group>

            <Form.Group className="mb-3" controlId="formBasicPassword">
              <Form.Label>Password</Form.Label>
              <Form.Control
                type="password"
                onChange={(e) => {
                  setPassword(e.target.value);
                }}
                placeholder="Password"
              />
            </Form.Group>
            <Button variant="primary" type="submit">
              Submit
            </Button>
          </Form>
        </Col>
        <Col></Col>
      </Row>
      <Row>
        <Col>
          <span color="red">{error}</span>
        </Col>
      </Row>
    </>
  );
};
