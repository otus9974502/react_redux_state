import { Col, Row } from "react-bootstrap";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import { useDispatch } from "react-redux";
import { Actions } from "../../state/reducers/userReducer";
import { useState } from "react";
import { User } from "../../models/user";
import { useNavigate } from "react-router-dom";

interface formData {
  login: string;
  usermame: string;
  password: string;
}

export const RegisterPage = () => {
  //регистрируем state
  const [login, setLogin] = useState("");
  const [username, setUserName] = useState("");
  const [password, setPassword] = useState("");

  const navigate = useNavigate();

  //регистрируем диспетчер
  const dispatch = useDispatch();

  // submit
  const formSubmit = (event: any) => {
    event.preventDefault();

    const newUser: User = {
      login: login,
      username: username,
      password: password,
    };

    dispatch(Actions.registerUser(newUser));

    navigate("/");
  };

  return (
    <Row>
      <Col></Col>
      <Col>
        <Form onSubmit={formSubmit}>
          <Form.Group className="mb-3" controlId="formBasicName">
            <Form.Label>Name</Form.Label>
            <Form.Control
              type="name"
              value={username}
              onChange={(e) => {
                setUserName(e.target.value);
              }}
              placeholder="Enter name"
            />
          </Form.Group>

          <Form.Group className="mb-3" controlId="formBasicEmail">
            <Form.Label>Email address</Form.Label>
            <Form.Control
              type="email"
              value={login}
              onChange={(e) => {
                setLogin(e.target.value);
              }}
              placeholder="Enter email"
            />
          </Form.Group>

          <Form.Group className="mb-3" controlId="formBasicPassword">
            <Form.Label>Password</Form.Label>
            <Form.Control
              type="password"
              value={password}
              onChange={(e) => {
                setPassword(e.target.value);
              }}
              placeholder="Password"
            />
          </Form.Group>
          <Button variant="primary" type="submit">
            Submit
          </Button>
        </Form>
      </Col>
      <Col></Col>
    </Row>
  );
};
