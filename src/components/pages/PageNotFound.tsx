import { Row, Col } from "react-bootstrap";
import notfound from "../../notfound.jpeg";

export const PageNotFound = () => {
  return (
    <Row>
      <Col></Col>
      <Col>
        <img src={notfound} width="200px"></img>
      </Col>
      <Col>
        <h3 className="text-center align-middle">
          Нам очень жаль, но вы ломитесь на несуществующую страницу
        </h3>
      </Col>
      <Col></Col>
    </Row>
  );
};
