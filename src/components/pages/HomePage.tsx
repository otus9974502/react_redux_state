import { useSelector } from "react-redux";
import { UsersStoreState } from "../../state/reducers/userReducer";
import { User } from "../../models/user";
import { Table } from "react-bootstrap";

export const HomePage = () => {
  const usersStore = useSelector((state: any) => state.usersStore.users);
  const loggedStore = useSelector((state: any) => state.loggedStore);

  return (
    <div>
      <div>Зарегистрированных пользователей: {usersStore.length}</div>
      {usersStore.length === 0 ? (
        <div>зарегистрируйте пользователей</div>
      ) : (
        <Table striped bordered hover>
          <thead>
            <tr>
              <th></th>
              <th>Login</th>
              <th>Name</th>
              <th>Password</th>
            </tr>
          </thead>
          <tbody>
            {usersStore.map((user: any, i: any) => {
              return (
                <tr id={"tr_" + i}>
                  <td>{i}</td>
                  <td>{user.login}</td>
                  <td>{user.username}</td>
                  <td>{user.password}</td>
                </tr>
              );
            })}
          </tbody>
        </Table>
      )}
      <hr />
      <div>Авторизованный пользователь:</div>
      {loggedStore.isLogged === true ? (
        <>
          <div>Login: {loggedStore.user.login}</div>
          <div>Username: {loggedStore.user.username}</div>
        </>
      ) : (
        <div>Пользователь не авторизован</div>
      )}
    </div>
  );
};
