import { Navbar, Container, Nav, NavDropdown, Row, Col } from "react-bootstrap";
import { BrowserRouter, Link } from "react-router-dom";
import { ReactComponent as Logo } from "../logo.svg";
import { useDispatch, useSelector } from "react-redux";
import { Actions } from "../state/reducers/loginReducer";

export const TopBar = () => {
  const loggedStore = useSelector((state: any) => state.loggedStore);
  const dispatch = useDispatch();

  const logout = () => {
    dispatch(Actions.logOutUser());
  };

  return (
    <Row>
      <Navbar expand="lg" className="bg-body-tertiary">
        <Container>
          <Navbar.Brand as={Link} to="/">
            <Logo width="48px" />
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse
            id="basic-navbar-nav"
            className="justify-content-end"
          >
            <Nav className="ml-auto">
              <Nav.Link as={Link} to="/register">
                Register
              </Nav.Link>
              <Navbar.Text> | </Navbar.Text>
              {loggedStore.isLogged === true ? (
                <>
                  <Navbar.Text> [{loggedStore.user.username}]</Navbar.Text>
                  <Nav.Link onClick={logout}>Logout</Nav.Link>
                </>
              ) : (
                <Nav.Link as={Link} to="/login">
                  Login
                </Nav.Link>
              )}
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </Row>
  );
};
