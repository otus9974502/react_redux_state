export interface User {
  login: string;
  username: string;
  password: string;
}
