import { configureStore } from "@reduxjs/toolkit";
import userReducer from "./reducers/userReducer";
import loginReducer from "./reducers/loginReducer";

const store = configureStore({
  reducer: {
    usersStore: userReducer,
    loggedStore: loginReducer,
  },
});

export default store;
