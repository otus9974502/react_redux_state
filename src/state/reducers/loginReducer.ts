import { User } from "../../models/user";

export const Actions = Object.freeze({
  loginUser: (user: User) => ({
    type: "USER_LOGIN",
    payload: user,
  }),
  logOutUser: () => ({
    type: "USER_LOGOUT",
  }),
});

interface UserLoggedState {
  user: User | null;
  isLogged: boolean;
}

const initialState: UserLoggedState = {
  user: null,
  isLogged: false,
};

const loginReducer = (state = initialState, action: any) => {
  switch (action.type) {
    case "USER_LOGIN":
      return { ...state, user: action.payload, isLogged: true };
    case "USER_LOGOUT":
      return { user: null, isLogged: false };
    default:
      return state;
  }
};
export default loginReducer;
