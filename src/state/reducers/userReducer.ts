import { User } from "../../models/user";

export const Actions = {
  registerUser: (user: User) => ({
    type: "REGISTER_USER",
    payload: user,
  }),
};

export interface UsersStoreState {
  users: User[];
}

const initialState: UsersStoreState = {
  users: [],
};

const userReducer = (state = initialState, action: any) => {
  switch (action.type) {
    case "REGISTER_USER":
      console.log(action.payload);
      return { ...state, users: [...state.users, action.payload] };
    default:
      return state;
  }
};

export default userReducer;
