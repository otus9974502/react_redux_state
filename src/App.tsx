import React from "react";
import "./App.css";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import { Col, Container, Row } from "react-bootstrap";
import { TopBar } from "./components/TopBar";
import { HomePage } from "./components/pages/HomePage";
import { LoginPage } from "./components/pages/LoginPage";
import { RegisterPage } from "./components/pages/RegisterPage";
import { PageNotFound } from "./components/pages/PageNotFound";

const App = () => {
  return (
    <BrowserRouter>
      <Container>
        <TopBar></TopBar>
        <Routes>
          <Route path="/" element={<HomePage />} />
          <Route path="/login" element={<LoginPage />} />
          <Route path="/register" element={<RegisterPage />} />

          <Route path="*" element={<PageNotFound />} />
        </Routes>
      </Container>
    </BrowserRouter>
  );
};

export default App;
